﻿using Entin.Test.Model.FSM;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Entin.Test.View
{
    internal class BackToTown : MonoBehaviour
    {
        [SerializeField]
        private Button button;

        private IStateMachine stateMachine;

        [Inject]
        public void Initialize(IStateMachine stateMachine)
        {
            this.stateMachine = stateMachine;
        }

        private void Start()
        {
            button.AddSingleAction(OnButtonClick);
        }

        private void OnButtonClick()
        {
            stateMachine.Event(StateMachineTransition.ToTown);
        }
    }
}