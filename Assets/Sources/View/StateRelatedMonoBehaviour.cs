﻿using Entin.Test.Model.FSM;
using UnityEngine;
using Zenject;

namespace Entin.Test.View
{
    internal abstract class StateRelatedMonoBehaviour<T> : MonoBehaviour
       where T : IState
    {
        protected IStateMachine stateMachine;

        [Inject]
        public void Initialize(IStateMachine stateMachine)
        {
            this.stateMachine = stateMachine;
        }

        protected virtual void Start()
        {
            stateMachine.OnStateChanged += OnStateChanged;
            OnStateChanged(stateMachine.CurrentState);
        }

        private void OnStateChanged(IState newState)
        {
            if (typeof(T).IsAssignableFrom(newState.GetType()))
                Enable();
            else
                Disable();
        }

        protected abstract void Enable();

        protected abstract void Disable();

        protected virtual void OnDestroy()
        {
            stateMachine.OnStateChanged -= OnStateChanged;
        }
    }
}