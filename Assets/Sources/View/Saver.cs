using Entin.Test.Model;
using Entin.Test.Model.Data;
using System;
using UnityEngine;
using Zenject;

namespace Entin.Test.View.Save
{
    public class Saver : MonoBehaviour
    {
        private ISave save;

        private Timer timer = new Timer(DateTime.UtcNow, TimeSpan.FromMinutes(1));

        [Inject]
        public void Initialize(ISave save)
        {
            this.save = save;
        }

        private void OnApplicationQuit()
        {
            save.SaveGame();
        }

        private void Update()
        {
            if (timer.IsReady)
            {
                save.SaveGame();
                timer.Shift();
            }
        }
    }
}