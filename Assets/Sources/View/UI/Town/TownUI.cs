﻿using Entin.Test.Model.Data;
using Entin.Test.Model.FSM;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Entin.Test.View.BuildingCanvas.Town
{
    internal class TownUI : StateRelatedCanvas<ITownState>
    {
        [SerializeField]
        private Button resetSave, quitGame;

        private ISave save;

        [Inject]
        public void Initialize(ISave save)
        {
            this.save = save;
        }

        protected override void Start()
        {
            base.Start();
            resetSave.AddSingleAction(ResetSave);
            quitGame.AddSingleAction(Quit);
        }

        private void ResetSave()
        {
            save.ResetGame();
        }

        private void Quit()
        {
            Application.Quit();
        }
    }
}