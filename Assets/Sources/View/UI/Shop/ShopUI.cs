using Entin.Test.Model.FSM;
using Entin.Test.Model.Shop;
using Entin.Test.View.BuildingCanvas.Item;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.UI;
using Zenject;

namespace Entin.Test.View.BuildingCanvas.Shop
{
    internal class ShopUI : StateRelatedCanvas<IShopState>
    {
        [SerializeField]
        private TextMeshProUGUI header, timerText;
        [SerializeField]
        private Button buyButton;
        [SerializeField]
        private ItemUI item;
        [SerializeField]
        private ItemUI selectedItemInfo;

        private ObjectPool<ItemUI> itemsPool;
        private readonly List<ItemUI> currentItems = new List<ItemUI>();

        private IShop shop;

        [Inject]
        public void Initialize(IShop shop)
        {
            this.shop = shop;
        }

        protected override void Start()
        {
            base.Start();
            item.gameObject.SetActive(false);
            itemsPool = new ObjectPool<ItemUI>(() => item.Clone(false), OnGetItemUI, OnReleaseItemUI);

            buyButton.onClick.AddListener(shop.BuyButtonPressed);

            shop.OnItemsUpdate += UpdateItems;
            UpdateItems();

            shop.OnCurrentItemUpdate += SelectedItemInfoChanged;
            SelectedItemInfoChanged();

            header.text = "Shop";
        }

        private void Update()
        {
            timerText.text = UIHelper.GetTimerFormat(shop.RefreshShopTimer, "Refresh in: ");
        }

        private void OnGetItemUI(ItemUI itemUI)
        {
            itemUI.gameObject.SetActive(true);
            itemUI.transform.SetAsLastSibling();
        }

        private void OnReleaseItemUI(ItemUI itemUI)
        {
            itemUI.gameObject.SetActive(false);
        }

        private void UpdateItems()
        {
            foreach (var item in currentItems)
            {
                itemsPool.Release(item);
            }
            currentItems.Clear();

            foreach (var item in shop.Items)
            {
                var receivedItem = itemsPool.Get();
                receivedItem.Initialize(item, () => shop.SelectItem(item));
                currentItems.Add(receivedItem);
            }
        }

        private void SelectedItemInfoChanged()
        {
            selectedItemInfo.gameObject.SetActive(shop.CurrentItem != null);

            if (shop.CurrentItem != null)
                selectedItemInfo.Initialize(shop.CurrentItem);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            shop.OnItemsUpdate -= UpdateItems;
        }

        public RectTransform GetFirstItemRectForTutorial()
        {
            return currentItems[0].transform as RectTransform;
        }
    }
}