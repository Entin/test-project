﻿using Entin.Test.Model.Farm;
using Entin.Test.View.BuildingCanvas.Item;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Entin.Test.View.BuildingCanvas.Farm
{
    internal class GrowProcessUI : MonoBehaviour
    {
        [SerializeField]
        private ItemUI source, result;
        [SerializeField]
        private TextMeshProUGUI timer;
        [SerializeField]
        private Button collectButton;

        private IGrowProcess growProcess;

        public void Initialize(IGrowProcess growProcess, Action onButtonClick)
        {
            this.growProcess = growProcess;

            collectButton.AddSingleAction(onButtonClick);

            source.Initialize(growProcess.FarmReceipt.Source);
            result.Initialize(growProcess.FarmReceipt.Result);
        }

        private void Update()
        {
            if (growProcess != null)
            {
                if (growProcess.Timer.IsReady)
                    timer.text = "Ready";
                else
                    timer.text = UIHelper.GetTimerFormat(growProcess.Timer);
            }
        }
    }
}