﻿using Entin.Test.Model.Farm;
using Entin.Test.View.BuildingCanvas.Item;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Entin.Test.View.BuildingCanvas.Farm
{
    internal class FarmReceiptUI : MonoBehaviour
    {
        [SerializeField]
        private ItemUI source, result;
        [SerializeField]
        private TextMeshProUGUI timer;
        [SerializeField]
        private Button button;

        public string Key { get; private set; }
        public RectTransform SourceItemRectTutorial => source.transform as RectTransform;
        public RectTransform ButtonRectTutorial => button.transform as RectTransform;

        public void Initialize(IFarmReceipt farmReceipt, Action onButtonPressed)
        {
            Key = farmReceipt.Key;

            source.Initialize(farmReceipt.Source);
            result.Initialize(farmReceipt.Result);

            timer.text = farmReceipt.GrowTime.ToString();

            button.AddSingleAction(onButtonPressed);
        }
    }
}