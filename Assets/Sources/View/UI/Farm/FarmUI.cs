using Entin.Test.Model.Farm;
using Entin.Test.Model.FSM;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

namespace Entin.Test.View.BuildingCanvas.Farm
{
    internal class FarmUI : StateRelatedCanvas<IFarmState>
    {
        [SerializeField]
        private TextMeshProUGUI header, selectSomethingText;
        [SerializeField]
        private GrowProcessUI growProcess;
        [SerializeField]
        private FarmReceiptUI farmReceipt;

        public IReadOnlyList<FarmReceiptUI> Receipts { get; private set; }

        private IFarm farm;

        [Inject]
        public void Initialize(IFarm farm)
        {
            this.farm = farm;
        }

        protected override void Start()
        {
            base.Start();
            farmReceipt.gameObject.SetActive(false);
            var receipts = new List<FarmReceiptUI>();
            foreach (var item in farm.Receipts)
            {
                var receipt = farmReceipt.Clone();
                receipt.Initialize(item, () => farm.StartGrow(item.Key));
                receipts.Add(receipt);
            }
            Receipts = receipts;
            farm.OnGrowProcessUpdate += OnGrowProcessUpdate;
            OnGrowProcessUpdate();
        }

        private void OnGrowProcessUpdate()
        {
            bool growingExist = farm.GrowProcess != null;

            selectSomethingText.gameObject.SetActive(!growingExist);
            growProcess.gameObject.SetActive(growingExist);

            if (growingExist)
                growProcess.Initialize(farm.GrowProcess, farm.CollectCurrentGrow);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            farm.OnGrowProcessUpdate += OnGrowProcessUpdate;
        }
    }
}