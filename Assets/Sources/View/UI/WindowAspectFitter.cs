using UnityEngine;
using UnityEngine.UI;

namespace Entin.Test.View
{
    internal class WindowAspectFitter : MonoBehaviour
    {
        private AspectRatioFitter fitter;

        private void Start()
        {
            fitter = gameObject.GetComponent<AspectRatioFitter>();
            if (fitter == null) fitter = gameObject.AddComponent<AspectRatioFitter>();
            fitter.aspectMode = AspectRatioFitter.AspectMode.FitInParent;
            Fit();
        }

        private void Fit()
        {
            fitter.aspectRatio = Mathf.Min((float)Screen.safeArea.width / Screen.safeArea.height, (1080f / 1920f));
        }

#if UNITY_EDITOR
        private void Update()
        {
            Fit();
        }
#endif
    }
}