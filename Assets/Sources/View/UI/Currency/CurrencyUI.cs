using Entin.Test.Model.Currency;
using TMPro;
using UnityEngine;
using Zenject;

namespace Entin.Test.View.Currency
{
    internal class CurrencyUI : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI goldText;

        private ICurrency currency;

        [Inject]
        public void Initialize(ICurrency currency)
        {
            this.currency = currency;
        }

        private void Start()
        {
            currency.OnGoldChange += OnGoldChange;
            OnGoldChange(currency.Gold);
        }

        private void OnGoldChange(int gold)
        {
            goldText.text = UIHelper.GetGoldFormat(gold);
        }

        private void OnDestroy()
        {
            currency.OnGoldChange -= OnGoldChange;
        }
    }
}
