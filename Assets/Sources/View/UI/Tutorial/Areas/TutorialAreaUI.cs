﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Entin.Test.View.Tutorial
{
    [RequireComponent(typeof(RectTransform))]
    internal abstract class TutorialAreaUI : MonoBehaviour
    {
        [SerializeField]
        private Button button;

        protected abstract TutorialAreaType Type { get; }

        public void Initialize(Action onClick)
        {
            button.AddSingleAction(onClick);
        }
    }
}