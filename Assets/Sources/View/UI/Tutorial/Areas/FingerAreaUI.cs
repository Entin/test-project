﻿using UnityEngine;

namespace Entin.Test.View.Tutorial
{
    internal class FingerAreaUI : TutorialAreaUI
    {
        [SerializeField]
        private RectTransform finger;

        [SerializeField]
        private float animationSpeed, animationDistance;

        protected override TutorialAreaType Type => TutorialAreaType.Finger;

        private Vector3 startPos;

        private void Start()
        {
            startPos = finger.position;
        }

        private void Update()
        {
            var y = (Mathf.Sin(Time.time * animationSpeed) + 1) * animationDistance;
            finger.position = startPos - new Vector3(0, y);
        }
    }
}