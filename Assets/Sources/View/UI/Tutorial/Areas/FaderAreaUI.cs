﻿namespace Entin.Test.View.Tutorial
{
    internal class FaderAreaUI : TutorialAreaUI
    {
        protected override TutorialAreaType Type => TutorialAreaType.Fader;
    }
}