using Entin.Test.Model.Tutorial;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Entin.Test.View.Tutorial
{
    internal class TutorialOverlayUI : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI message, tip;
        [SerializeField]
        private RectTransform panel, tipPanel, faderIn;
        [SerializeField]
        private FaderAreaUI faderArea;
        [SerializeField]
        private FingerAreaUI fingerArea;
        [SerializeField]
        private Button backgroundButton;

        private Canvas canvas;

        private readonly List<TutorialAreaUI> areas = new List<TutorialAreaUI>();

        private ITutorialOverlay tutorialOverlay;

        [Inject]
        public void Initialize(ITutorialOverlay tutorialOverlay)
        {
            this.tutorialOverlay = tutorialOverlay;
        }

        private void Start()
        {
            canvas = GetComponent<Canvas>();

            faderArea.gameObject.SetActive(false);
            fingerArea.gameObject.SetActive(false);

            backgroundButton.AddSingleAction(tutorialOverlay.Click);

            tutorialOverlay.OnMessageUpdate += TutorialMessageUpdate;
            tutorialOverlay.OnAreasUpdate += TutorialAreasUpdate;
            TutorialMessageUpdate();
            TutorialAreasUpdate();
        }

        private void TutorialAreasUpdate()
        {
            areas.ForEach(x => Destroy(x.gameObject));
            areas.Clear();

            foreach (var area in tutorialOverlay.Areas)
            {
                TutorialAreaUI tutorialAreaUI = null;

                if (area.AreaTutorialType == TutorialAreaType.Fader)
                    tutorialAreaUI = faderArea.Clone();
                else if (area.AreaTutorialType == TutorialAreaType.Finger)
                    tutorialAreaUI = fingerArea.Clone();

                if (tutorialAreaUI == null)
                    return;

                tutorialAreaUI.Initialize(area.Action);
                (tutorialAreaUI.transform as RectTransform).CopyRectTransform(area.Transform);
                areas.Add(tutorialAreaUI);
            }

            faderIn.SetAsLastSibling();
        }

        private void TutorialMessageUpdate()
        {
            canvas.enabled = tutorialOverlay.Active;

            message.text = tutorialOverlay.MessageKey;
            bool haveTip = !string.IsNullOrEmpty(tutorialOverlay.TipKey);
            tipPanel.gameObject.SetActive(haveTip);
            if (haveTip)
                tip.text = tutorialOverlay.TipKey;

            StartCoroutine(UpdatePosition());
        }

        IEnumerator UpdatePosition()
        {
            yield return new WaitForEndOfFrame();
            float max = (transform as RectTransform).rect.height - panel.rect.height;
            float yPosition = max * tutorialOverlay.Position;

            panel.anchoredPosition = new Vector2(0, yPosition);
        }

        private void OnDestroy()
        {
            tutorialOverlay.OnMessageUpdate -= TutorialMessageUpdate;
            tutorialOverlay.OnAreasUpdate -= TutorialAreasUpdate;
        }

    }
}