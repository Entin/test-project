﻿using Entin.Test.Model.FSM;
using Entin.Test.Model.Storage;
using Entin.Test.Model.Tutorial;
using Entin.Test.View.BuildingCanvas.Storage;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Entin.Test.View.Tutorial
{
    [RequireComponent(typeof(StorageUI))]
    internal class StorageTutorial : StateRelatedTutorial<IStorageState>
    {
        [SerializeField]
        private RectTransform itemsRect;

        private TutorialArea itemsFader;

        protected override TutorialType Type => TutorialType.Storage;

        private IStorage storage;
        private StorageUI storageUI;

        private Dictionary<int, Action> tutorialMethods;

        [Inject]
        public void Initialize(IStorage storage)
        {
            this.storage = storage;
        }

        protected override void Start()
        {
            storageUI = GetComponent<StorageUI>();

            itemsFader = new TutorialArea(itemsRect, TutorialAreaType.Fader, NextStage);

            tutorialMethods = new Dictionary<int, Action>()
            {
                { 0, Stage_0 },
                { 1, Stage_1 },
                { 2, Stage_2 },
                { 3, Stage_3 },
            };

            base.Start();
        }

        private void SelectFirstItem()
        {
            NextStage();
            storage.SellFirstItemTutorial();
        }

        protected override void OnTutorialStageChanged(TutorialStage stage)
        {
            if (stage == null)
                return;

            if (!tutorialMethods.ContainsKey(stage.Stage))
                return;

            tutorialMethods[stage.Stage].Invoke();
        }

        private void Stage_0()
        {
            Send(NextStage, itemsFader);
        }

        private void Stage_1()
        {
            Send(NextStage, itemsFader);
        }

        private void Stage_2()
        {
            var firstItemRect = storageUI.GetFirstItemRectForTutorial();
            var firstItemFader = new TutorialArea(firstItemRect, TutorialAreaType.Fader, SelectFirstItem);
            var firstItemFinger = new TutorialArea(firstItemRect, TutorialAreaType.Finger, SelectFirstItem);

            Send(firstItemFader, firstItemFinger);
        }

        private void Stage_3()
        {
            Send(NextStage);
        }
    }
}
