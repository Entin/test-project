﻿using Entin.Test.Model.Farm;
using Entin.Test.Model.FSM;
using Entin.Test.Model.Tutorial;
using Entin.Test.View.BuildingCanvas.Farm;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Entin.Test.View.Tutorial
{
    [RequireComponent(typeof(FarmUI))]
    internal class FarmTutorial : StateRelatedTutorial<IFarmState>
    {
        [SerializeField]
        private RectTransform collectButtonRect, currentGrowRect;

        private TutorialArea collectButtonFader, collectButtonFinger, currentGrowFader;
        private bool Growing => farm.GrowProcess == null || !farm.GrowProcess.Timer.IsReady;

        protected override TutorialType Type => TutorialType.Farm;

        private IFarm farm;
        private FarmUI farmUI;

        private Dictionary<int, Action> tutorialMethods;

        [Inject]
        public void Initialize(IFarm farm)
        {
            this.farm = farm;
        }

        protected override void Start()
        {
            farmUI = GetComponent<FarmUI>();

            collectButtonFader = new TutorialArea(collectButtonRect, TutorialAreaType.Fader, CollectButtonPressed);
            collectButtonFinger = new TutorialArea(collectButtonRect, TutorialAreaType.Finger, CollectButtonPressed);
            currentGrowFader = new TutorialArea(currentGrowRect, TutorialAreaType.Fader, NextAndDisable);

            tutorialMethods = new Dictionary<int, Action>()
            {
                { 0, Stage_0 },
                { 1, Stage_1 },
                { 2, Stage_2 },
                { 3, Stage_3 },
                { 4, Stage_4 },
            };

            base.Start();
        }

        protected override void Enable()
        {
            if (tutorial.GetStage(Type) == 4 && Growing)
                return;

            base.Enable();
        }

        private void Update()
        {
            if (tutorial.GetStage(Type) == 4 && !Growing && !tutorial.Active(Type))
            {
                if (stateMachine.CurrentState is IFarmState)
                    Enable();
            }
        }

        protected override void OnTutorialStageChanged(TutorialStage stage)
        {
            if (stage == null)
                return;

            if (!tutorialMethods.ContainsKey(stage.Stage))
                return;

            tutorialMethods[stage.Stage].Invoke();
        }

        private void Stage_0()
        {
            Send(NextStage);
        }

        private void Stage_1()
        {
            var receipt = farmUI.Receipts.First();
            var area = new TutorialArea(receipt.SourceItemRectTutorial, TutorialAreaType.Fader, NextStage);
            Send(NextStage, area);
        }

        private void Stage_2()
        {
            var areas = new List<TutorialArea>();

            foreach (var item in farmUI.Receipts)
            {
                areas.Add(new TutorialArea(item.ButtonRectTutorial, TutorialAreaType.Fader, () => OnGrowPressed(item.Key)));
                areas.Add(new TutorialArea(item.ButtonRectTutorial, TutorialAreaType.Finger, () => OnGrowPressed(item.Key)));
            }

            Send(areas);
        }

        private void OnGrowPressed(string receiptKey)
        {
            NextStage();
            farm.TutorialStartGrow(receiptKey);
        }

        private void Stage_3()
        {
            Send(NextAndDisable, currentGrowFader);
        }

        private void NextAndDisable()
        {
            NextStage();
            if (Growing)
                Disable();
        }

        private void Stage_4()
        {
            Send(collectButtonFader, collectButtonFinger);
        }

        private void CollectButtonPressed()
        {
            NextStage();
            farm.CollectCurrentGrow();
        }
    }
}
