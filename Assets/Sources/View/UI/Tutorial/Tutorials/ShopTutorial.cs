﻿using Entin.Test.Model.FSM;
using Entin.Test.Model.Shop;
using Entin.Test.Model.Tutorial;
using Entin.Test.View.BuildingCanvas.Shop;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Entin.Test.View.Tutorial
{
    [RequireComponent(typeof(ShopUI))]
    internal class ShopTutorial : StateRelatedTutorial<IShopState>
    {
        [SerializeField]
        private RectTransform itemsRect, timerRect;

        private TutorialArea itemsFader, timerFader;

        protected override TutorialType Type => TutorialType.Shop;

        private IShop shop;
        private ShopUI shopUI;

        private Dictionary<int, Action> tutorialMethods;

        [Inject]
        public void Initialize(IShop shop)
        {
            this.shop = shop;
        }

        protected override void Start()
        {
            shopUI = GetComponent<ShopUI>();

            itemsFader = new TutorialArea(itemsRect, TutorialAreaType.Fader, NextStage);
            timerFader = new TutorialArea(timerRect, TutorialAreaType.Fader, NextStage);

            tutorialMethods = new Dictionary<int, Action>()
            {
                { 0, Stage_0 },
                { 1, Stage_1 },
                { 2, Stage_2 },
                { 3, Stage_3 },
            };

            base.Start();
        }

        private void SelectFirstItem()
        {
            NextStage();
            shop.SelectFirstItemTutorial();
        }

        protected override void OnTutorialStageChanged(TutorialStage stage)
        {
            if (stage == null)
                return;

            if (!tutorialMethods.ContainsKey(stage.Stage))
                return;

            tutorialMethods[stage.Stage].Invoke();
        }

        private void Stage_0()
        {
            Send(NextStage, itemsFader);
        }

        private void Stage_1()
        {
            Send(NextStage, timerFader);
        }

        private void Stage_2()
        {
            var firstItemRect = shopUI.GetFirstItemRectForTutorial();
            var firstItemFader = new TutorialArea(firstItemRect, TutorialAreaType.Fader, SelectFirstItem);
            var firstItemFinger = new TutorialArea(firstItemRect, TutorialAreaType.Finger, SelectFirstItem);

            Send(firstItemFader, firstItemFinger);
        }

        private void Stage_3()
        {
            Send(NextStage);
        }
    }
}
