﻿using Entin.Test.Model.FSM;
using Entin.Test.Model.Tutorial;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Entin.Test.View.Tutorial
{
    internal class TownTutorial : StateRelatedTutorial<ITownState>
    {
        [SerializeField]
        private RectTransform allBuildings, shopBuilding, storageBuilding, farmBuilding, goldRect;

        private TutorialArea allBuildingsFader,
            shopBuildingFader, shopBuildingFinger,
            storageBuildingFader, storageBuildingFinger,
            farmBuildingFader, farmBuildingFinger,
            goldRectFader;

        protected override TutorialType Type => TutorialType.Town;

        private Dictionary<int, Action> tutorialMethods;

        protected override void Start()
        {
            allBuildingsFader = new TutorialArea(allBuildings, TutorialAreaType.Fader, NextStage);
            shopBuildingFader = new TutorialArea(shopBuilding, TutorialAreaType.Fader, OpenShop);
            shopBuildingFinger = new TutorialArea(shopBuilding, TutorialAreaType.Finger, OpenShop);
            storageBuildingFader = new TutorialArea(storageBuilding, TutorialAreaType.Fader, OpenStorage);
            storageBuildingFinger = new TutorialArea(storageBuilding, TutorialAreaType.Finger, OpenStorage);
            farmBuildingFader = new TutorialArea(farmBuilding, TutorialAreaType.Fader, OpenFarm);
            farmBuildingFinger = new TutorialArea(farmBuilding, TutorialAreaType.Finger, OpenFarm);
            goldRectFader = new TutorialArea(goldRect, TutorialAreaType.Fader, NextStage);

            tutorialMethods = new Dictionary<int, Action>()
            {
                { 0, Stage_0 },
                { 1, Stage_1 },
                { 2, Stage_2 },
                { 3, Stage_3 },
                { 4, Stage_4 },
                { 5, Stage_5 },
                { 6, Stage_6 },
            };

            base.Start();
        }

        protected override void OnTutorialStageChanged(TutorialStage stage)
        {
            if (stage == null)
                return;

            if (!tutorialMethods.ContainsKey(stage.Stage))
                return;

            tutorialMethods[stage.Stage].Invoke();
        }

        private void Stage_0()
        {
            Send(NextStage);
        }

        private void Stage_1()
        {
            Send(NextStage, allBuildingsFader);
        }

        private void Stage_2()
        {
            Send(NextStage, goldRectFader);
        }

        private void Stage_3()
        {
            Send(shopBuildingFader, shopBuildingFinger);
        }

        private void Stage_4()
        {
            Send(farmBuildingFader, farmBuildingFinger);
        }

        private void Stage_5()
        {
            Send(storageBuildingFader, storageBuildingFinger);
        }

        private void Stage_6()
        {
            Send(NextStage);
        }

        private void OpenShop()
        {
            NextStage();
            stateMachine.Event(StateMachineTransition.ToShop);
        }

        private void OpenStorage()
        {
            NextStage();
            stateMachine.Event(StateMachineTransition.ToStorage);
        }

        private void OpenFarm()
        {
            NextStage();
            stateMachine.Event(StateMachineTransition.ToFarm);
        }
    }
}
