using Entin.Test.Model.FSM;
using Entin.Test.Model.Tutorial;
using System;
using System.Collections.Generic;
using Zenject;

namespace Entin.Test.View.Tutorial
{
    internal abstract class StateRelatedTutorial<T> : StateRelatedMonoBehaviour<T>
        where T : IState
    {
        protected ITutorial tutorial;
        private ITutorialOverlay tutorialOverlay;

        protected abstract TutorialType Type { get; }

        [Inject]
        public void Initialize(ITutorial tutorial, ITutorialOverlay tutorialOverlay)
        {
            this.tutorial = tutorial;
            this.tutorialOverlay = tutorialOverlay;
        }

        protected override void Start()
        {
            base.Start();
            tutorial.OnTutorialStageChanged += OnTutorialStageChanged;
            tutorial.UpdateCurrentTutorial();
        }

        private void OnTutorialStageChanged(TutorialType tutorial, TutorialStage stage)
        {
            if (Type == tutorial)
                OnTutorialStageChanged(stage);
        }

        protected override void Enable()
        {
            tutorial.Start(Type);
        }

        protected override void Disable()
        {
            tutorial.Stop(Type);
        }

        protected abstract void OnTutorialStageChanged(TutorialStage stage);

        protected override void OnDestroy()
        {
            base.OnDestroy();
            tutorial.OnTutorialStageChanged -= OnTutorialStageChanged;
        }

        protected void Send(Action onClick, params TutorialArea[] areas)
        {
            tutorialOverlay.SetAreas(areas, onClick);
        }

        protected void Send(Action onClick, List<TutorialArea> areas)
        {
            tutorialOverlay.SetAreas(areas, onClick);
        }

        protected void Send(params TutorialArea[] areas)
        {
            tutorialOverlay.SetAreas(areas, null);
        }

        protected void Send(List<TutorialArea> areas)
        {
            tutorialOverlay.SetAreas(areas, null);
        }

        protected void NextStage()
        {
            tutorial.NextStage(Type);
        }
    }
}
