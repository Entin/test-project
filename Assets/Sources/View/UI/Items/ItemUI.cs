﻿using Entin.Test.Model.Items;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Entin.Test.View.BuildingCanvas.Item
{
    internal class ItemUI : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI nameText, descriptionText, priceText;
        [SerializeField]
        private Image image;
        [SerializeField]
        private Button button;

        public void Initialize(IItem item, Action onClick = null)
        {
            if (nameText != null)
                nameText.text = item.NameKey;
            if (descriptionText != null)
                descriptionText.text = item.DescriptionKey;
            image.sprite = item.Sprite;

            if (button != null)
                button.AddSingleAction(onClick);

            priceText.text = UIHelper.GetGoldFormat(item.Price);
        }
    }
}