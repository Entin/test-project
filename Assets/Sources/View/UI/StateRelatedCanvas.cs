using Entin.Test.Model.FSM;
using UnityEngine;

namespace Entin.Test.View.BuildingCanvas
{
    [RequireComponent(typeof(Canvas))]
    internal abstract class StateRelatedCanvas<T> : StateRelatedMonoBehaviour<T>
        where T : IState
    {
        private Canvas canvas;

        protected virtual void Awake()
        {
            canvas = GetComponent<Canvas>();
        }

        protected override void Enable()
        {
            canvas.enabled = true;
        }

        protected override void Disable()
        {
            canvas.enabled = false;
        }
    }
}