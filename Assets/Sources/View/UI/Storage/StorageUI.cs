using Entin.Test.Model.FSM;
using Entin.Test.Model.Storage;
using Entin.Test.View.BuildingCanvas.Item;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Pool;
using Zenject;

namespace Entin.Test.View.BuildingCanvas.Storage
{
    internal class StorageUI : StateRelatedCanvas<IStorageState>
    {
        [SerializeField]
        private TextMeshProUGUI header;
        [SerializeField]
        private ItemUI item;

        private ObjectPool<ItemUI> itemsPool;
        private readonly List<ItemUI> currentItems = new List<ItemUI>();

        private IStorage storage;

        [Inject]
        public void Initialize(IStorage storage)
        {
            this.storage = storage;
        }

        protected override void Start()
        {
            base.Start();
            item.gameObject.SetActive(false);
            itemsPool = new ObjectPool<ItemUI>(() => item.Clone(false), OnGetItemUI, OnReleaseItemUI);

            storage.OnItemsUpdate += UpdateItems;
            UpdateItems();

            header.text = "Storage";
        }

        private void OnGetItemUI(ItemUI itemUI)
        {
            itemUI.gameObject.SetActive(true);
            itemUI.transform.SetAsLastSibling();
        }

        private void OnReleaseItemUI(ItemUI itemUI)
        {
            itemUI.gameObject.SetActive(false);
        }

        private void UpdateItems()
        {
            foreach (var item in currentItems)
            {
                itemsPool.Release(item);
            }
            currentItems.Clear();

            foreach (var item in storage.Items)
            {
                var receivedItem = itemsPool.Get();
                receivedItem.Initialize(item, () => storage.SellItem(item));
                currentItems.Add(receivedItem);
            }
        }

        public RectTransform GetFirstItemRectForTutorial()
        {
            return currentItems.First().transform as RectTransform;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            storage.OnItemsUpdate -= UpdateItems;
        }
    }
}