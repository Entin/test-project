using Entin.Test.Model.FSM;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Entin.Test.View
{
    internal class OpenBuldingButton : MonoBehaviour
    {
        [SerializeField]
        private StateMachineTransition transitionTo;

        private IStateMachine stateMachine;

        [Inject]
        public void Initialize(IStateMachine stateMachine)
        {
            this.stateMachine = stateMachine;
        }

        private void OnMouseDown()
        {
            if (!IsPointerOverGameObject())
                stateMachine.Event(transitionTo);
        }

        public bool IsPointerOverGameObject()
        {
            //check mouse
            if (EventSystem.current.IsPointerOverGameObject())
                return true;

            //check touch
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
    }
}