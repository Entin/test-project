using Entin.Test.Model;

namespace Entin.Test.View
{
    public static class UIHelper
    {
        public static string GetTimerFormat(Timer timer, string prefix = null)
        {
            var timeLeft = timer.TimeLeft;
            int hours = (int)timeLeft.TotalHours;
            int min = timeLeft.Minutes;
            int sec = timeLeft.Seconds;

            return $"{prefix}{hours:00}:{min:00}:{sec:00}";
        }

        public static string GetGoldFormat(int value)
        {
            return $"<sprite name=gold>{value}";
        }
    }
}
