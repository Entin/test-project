﻿using System;
using System.Collections.Generic;

namespace Entin.Test.Model.Tutorial
{
    public interface ITutorialOverlay
    {
        bool Active { get; }
        IReadOnlyList<TutorialArea> Areas { get; }
        float Position { get; }
        string MessageKey { get; }
        string TipKey { get; }
        event Action OnMessageUpdate;
        event Action OnAreasUpdate;

        void SetAreas(IEnumerable<TutorialArea> areas, Action onClick);
        void Click();
    }

    internal class TutorialOverlay : ITutorialOverlay
    {
        public bool Active { get; private set; }

        private List<TutorialArea> areas = new List<TutorialArea>();
        public IReadOnlyList<TutorialArea> Areas => areas;

        public float Position { get; private set; }
        public string MessageKey { get; private set; }
        public string TipKey { get; private set; }

        public event Action OnMessageUpdate;
        public event Action OnAreasUpdate;

        private Action onClick;

        public void Set(TutorialStage tutorialStage)
        {
            this.areas.Clear();

            Position = tutorialStage.Position;
            MessageKey = tutorialStage.MessageKey;
            TipKey = tutorialStage.TipKey;

            Active = true;
            OnMessageUpdate?.Invoke();
            OnAreasUpdate?.Invoke();
        }

        public void SetAreas(IEnumerable<TutorialArea> areas, Action onClick)
        {
            this.areas.Clear();
            this.areas.AddRange(areas);
            this.onClick = onClick;
            OnAreasUpdate?.Invoke();
        }

        public void Stop()
        {
            Active = false;
            OnMessageUpdate?.Invoke();
        }

        public void Click()
        {
            onClick?.Invoke();
        }
    }
}