﻿using System;
using UnityEngine;

namespace Entin.Test.Model.Tutorial
{
    public class TutorialArea
    {
        public RectTransform Transform { get; }
        public TutorialAreaType AreaTutorialType { get; }
        public Action Action { get; }

        public TutorialArea(RectTransform transform, TutorialAreaType areaTutorialType, Action action = null)
        {
            Transform = transform;
            AreaTutorialType = areaTutorialType;
            Action = action;
        }
    }
}