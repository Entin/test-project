﻿namespace Entin.Test.Model.Tutorial
{
    public class TutorialStage
    {
        public int Stage { get; }
        public string MessageKey { get; }
        public string TipKey { get; }
        public float Position { get; }

        public TutorialStage(int stage, string messageKey, string tipKey, float position)
        {
            Stage = stage;
            MessageKey = messageKey;
            TipKey = tipKey;
            Position = position;
        }
    }
}