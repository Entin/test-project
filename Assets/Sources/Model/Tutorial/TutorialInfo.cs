﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Entin.Test.Model.Tutorial
{
    internal class TutorialInfo
    {
        public TutorialType Type { get; }

        private readonly List<TutorialStage> stages;
        public IReadOnlyList<TutorialStage> Stages => stages;

        public TutorialStage Current { get; private set; }

        public int Priority { get; }
        public bool Completed => Current == null;

        public bool Active { get; set; }

        public TutorialInfo(TutorialType type, int priority, IEnumerable<TutorialStage> stages, int currentStage)
        {
            Type = type;
            Priority = priority;
            this.stages = new List<TutorialStage>(stages);
            Current = this.stages.FirstOrDefault(x => x.Stage == currentStage);
        }

        public int GetStage()
        {
            return stages.IndexOf(Current);
        }

        public void NextStage(int count)
        {
            SetStage(stages.IndexOf(Current) + count);
        }

        public void SetStage(int stage)
        {
            if (stage >= stages.Count || stage < 0)
                Current = null;
            else Current = stages[stage];
        }
    }
}