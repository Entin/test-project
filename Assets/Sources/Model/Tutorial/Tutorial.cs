using System.Collections.Generic;
using System.Linq;

namespace Entin.Test.Model.Tutorial
{
    public delegate void TutorialStageChangedDelegate(TutorialType tutorial, TutorialStage stage);

    public interface ITutorial
    {
        event TutorialStageChangedDelegate OnTutorialStageChanged;

        bool Active(TutorialType type);
        int GetStage(TutorialType type);
        void NextStage(TutorialType type);

        void Start(TutorialType type);
        void Stop(TutorialType type);
        void UpdateCurrentTutorial();
    }

    internal class Tutorial : ITutorial
    {
        public event TutorialStageChangedDelegate OnTutorialStageChanged;
        public readonly Dictionary<TutorialType, TutorialInfo> Infos = new Dictionary<TutorialType, TutorialInfo>();

        private TutorialOverlay tutorialOverlay;
        private Settings.GameSettings gameSettings;

        public Tutorial(TutorialOverlay tutorialOverlay, Settings.GameSettings gameSettings)
        {
            this.tutorialOverlay = tutorialOverlay;
            this.gameSettings = gameSettings;

            Infos.Clear();

            foreach (var item in gameSettings.Tutorial)
            {
                var stages = item.Stages.Select(x => new TutorialStage(x.Stage, x.MessageKey, x.TipKey, x.Position));
                Infos.Add(item.Type, new TutorialInfo(item.Type, item.Priority, stages, 0));
            }
        }

        public void SetData(Dictionary<int, int> infos)
        {
            foreach (var item in Infos)
            {
                if (infos.ContainsKey((int)item.Key))
                    item.Value.SetStage(infos[(int)item.Key]);
            }

            UpdateCurrentTutorial();
        }

        public void DefaultInitialize()
        {
            foreach (var item in Infos)
                item.Value.SetStage(0);

            UpdateCurrentTutorial();
        }

        public void Start(TutorialType type)
        {
            Infos[type].Active = true;
            UpdateCurrentTutorial();
        }

        public void Stop(TutorialType type)
        {
            Infos[type].Active = false;
            UpdateCurrentTutorial();
        }

        public void UpdateCurrentTutorial()
        {
            var activeInfos = Infos.Select(x => x.Value).Where(x => x.Active && !x.Completed);
            if (activeInfos.Any())
            {
                var prioritizedInfos = activeInfos.OrderBy(x => x.Priority).First();
                TutorialStage stage = prioritizedInfos.Current;
                Set(prioritizedInfos.Type, stage);
            }
            else
            {
                tutorialOverlay.Stop();
            }
        }

        public void Set(TutorialType type, TutorialStage stage)
        {
            tutorialOverlay.Set(stage);
            OnTutorialStageChanged?.Invoke(type, stage);
        }

        public void NextStage(TutorialType type)
        {
            Infos[type].NextStage(1);
            UpdateCurrentTutorial();
        }

        public int GetStage(TutorialType type)
        {
            return Infos[type].GetStage();
        }

        public bool Active(TutorialType type)
        {
            return Infos[type].Active;
        }
    }
}