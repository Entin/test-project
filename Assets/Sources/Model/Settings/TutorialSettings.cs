﻿using System.Collections.Generic;
using UnityEngine;

namespace Entin.Test.Model.Settings
{
    [System.Serializable]
    internal class TutorialSettings
    {
        [SerializeField]
        private TutorialType type;
        public TutorialType Type => type;

        [SerializeField]
        private int priority;
        public int Priority => priority;

        [SerializeField]
        private List<TutorialStageSettings> stages;
        public IReadOnlyList<TutorialStageSettings> Stages => stages;
    }
}