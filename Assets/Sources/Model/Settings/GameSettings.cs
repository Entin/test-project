using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Entin.Test.Model.Settings
{
    [CreateAssetMenu(fileName = "GameSettings")]
    internal class GameSettings : ScriptableObject
    {
        [SerializeField]
        private List<ItemsSettings> items;
        public IReadOnlyList<ItemsSettings> Items => items;

        [SerializeField]
        private List<FarmReceiptSettings> farmReceipts;
        public IReadOnlyList<FarmReceiptSettings> FarmReceipts => farmReceipts;

        [SerializeField]
        private List<TutorialSettings> tutorial;
        public IReadOnlyList<TutorialSettings> Tutorial => tutorial;

        public void Validate()
        {
            foreach (var receipt in farmReceipts)
            {
                ValidateItem(receipt.SourceItemKey, "FarmReceiptSettings: SourceItemKey");
                ValidateItem(receipt.ResultItemKey, "FarmReceiptSettings: ResultItemKey");
            }

            Debug.Log("Validation done");

            void ValidateItem(string itemKey, string source)
            {
                if (items.All(x => x.Key != itemKey))
                    Debug.LogError($"No one item with key '{itemKey}'. Source '{source}'");
            }
        }
    }
}