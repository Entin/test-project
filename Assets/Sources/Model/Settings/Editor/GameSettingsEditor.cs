using Entin.Test.Model.Settings;
using UnityEditor;
using UnityEngine;

namespace Entin.Test.Editor
{
#if UNITY_EDITOR
    [CustomEditor(typeof(GameSettings))]
    public class GameSettingsEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var component = (GameSettings)target;

            if (GUILayout.Button("Validate"))
            {
                component.Validate();
            }
        }
    }
#endif
}
