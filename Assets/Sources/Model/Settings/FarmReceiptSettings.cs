﻿using UnityEngine;

namespace Entin.Test.Model.Settings
{
    [System.Serializable]
    internal class FarmReceiptSettings
    {
        [SerializeField]
        private string key;
        public string Key => key;

        [SerializeField]
        private string sourceItemKey;
        public string SourceItemKey => sourceItemKey;

        [SerializeField]
        private string resultItemKey;
        public string ResultItemKey => resultItemKey;

        [SerializeField]
        private float growTimeSeconds;
        public float GrowTimeSeconds => growTimeSeconds;
    }
}