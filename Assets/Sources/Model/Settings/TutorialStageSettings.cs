﻿using UnityEngine;

namespace Entin.Test.Model.Settings
{
    [System.Serializable]
    internal class TutorialStageSettings
    {
        [SerializeField]
        private int stage;
        public int Stage => stage;

        [SerializeField]
        [TextArea]
        private string messageKey;
        public string MessageKey => messageKey;

        [SerializeField]
        [TextArea]
        private string tipKey;
        public string TipKey => tipKey;

        [SerializeField]
        [Range(0, 1)]
        private float position;
        public float Position => position;
    }
}