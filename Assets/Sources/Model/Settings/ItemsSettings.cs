﻿using UnityEngine;

namespace Entin.Test.Model.Settings
{
    [System.Serializable]
    internal class ItemsSettings
    {
        [SerializeField]
        private string key;
        public string Key => key;

        [SerializeField]
        private string nameKey;
        public string NameKey => nameKey;

        [SerializeField]
        private string descriptionKey;
        public string DescriptionKey => descriptionKey;

        [SerializeField]
        private int price;
        public int Price => price;

        [SerializeField]
        private Sprite sprite;
        public Sprite Sprite => sprite;
    }
}