using Entin.Test.Model.Data;
using Entin.Test.Model.FSM;
using Entin.Test.Model.Settings;
using Zenject;

namespace Entin.Test.Model.Installers
{
    internal class GameModelInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<GameSettings>().FromScriptableObjectResource("Settings/GameSettings").AsSingle();
            Container.BindInterfacesAndSelfTo<Shop.Shop>().AsSingle();
            Container.BindInterfacesAndSelfTo<Farm.Farm>().AsSingle();
            Container.BindInterfacesAndSelfTo<Storage.Storage>().AsSingle();
            Container.BindInterfacesAndSelfTo<Currency.Currency>().AsSingle();
            InstallTutorial();
            InstallSave();
            InstallStateMachine();
        }

        private void InstallTutorial()
        {
            Container.BindInterfacesAndSelfTo<Tutorial.Tutorial>().AsSingle();
            Container.BindInterfacesAndSelfTo<Tutorial.TutorialOverlay>().AsSingle();
        }

        private void InstallSave()
        {
            Container.BindInterfacesTo<Save>().AsSingle();
            Container.Bind<IFileProvider>().To<JsonFileProvider>().AsSingle();
        }

        private void InstallStateMachine()
        {
            StateMachine stateMachine = new StateMachine();

            stateMachine.Add(new TownState());
            stateMachine.Add(new ShopState());
            stateMachine.Add(new FarmState());
            stateMachine.Add(new StorageState());

            stateMachine.Start(typeof(TownState));

            Container.Bind<IStateMachine>().FromInstance(stateMachine);
        }
    }
}
