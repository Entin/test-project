using Entin.Test.Model.Items;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Entin.Test.Model.Storage
{
    public interface IStorage
    {
        IReadOnlyList<IItem> Items { get; }
        event Action OnItemsUpdate;

        void SellItem(IItem item);
        void SellFirstItemTutorial();
    }

    internal class Storage : IStorage
    {
        private readonly List<Item> items = new List<Item>();
        public IReadOnlyList<IItem> Items => items;

        public event Action OnItemsUpdate;

        private Currency.Currency currency;

        public Storage(Currency.Currency currency)
        {
            this.currency = currency;
        }

        public void SetData(IEnumerable<Item> newItems)
        {
            items.Clear();
            items.AddRange(newItems);
            OnItemsUpdate?.Invoke();
        }

        public void DefaultInitialize(Item tutorialItem)
        {
            items.Clear();
            items.Add(tutorialItem);
            OnItemsUpdate?.Invoke();
        }

        public void AddItem(Item item)
        {
            items.Add(item);
            OnItemsUpdate?.Invoke();
        }

        public void RemoveItem(string key)
        {
            var itemToRemove = items.FirstOrDefault(x => x.Key == key);
            if (itemToRemove == null)
                throw new InvalidOperationException("You try remove not existing item");

            items.Remove(itemToRemove);
            OnItemsUpdate?.Invoke();
        }

        public void SellItem(IItem item)
        {
            var itemToRemove = items.FirstOrDefault(x => x == item);
            if (itemToRemove == null)
                throw new InvalidOperationException("You try remove not existing item");

            currency.AddGold(itemToRemove.Price);

            items.Remove(itemToRemove);
            OnItemsUpdate?.Invoke();
        }

        public void SellFirstItemTutorial()
        {
            SellItem(items.First());
        }
    }
}
