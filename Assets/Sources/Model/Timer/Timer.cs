using System;

namespace Entin.Test.Model
{
    public class Timer
    {
        public DateTime LastTime { get; private set; }
        public TimeSpan Interval { get; private set; }
        public DateTime ReadyTime => LastTime + Interval;
        public TimeSpan TimeLeft => DateTime.UtcNow > ReadyTime ? TimeSpan.Zero : ReadyTime - DateTime.UtcNow;

        public bool IsReady => LastTime + Interval <= DateTime.UtcNow;

        public Timer(DateTime lastTime, TimeSpan interval)
        {
            LastTime = lastTime;
            Interval = interval;
        }

        public void Shift()
        {
            if (IsReady)
            {
                TimeSpan timeElapsed = DateTime.UtcNow < LastTime ? TimeSpan.Zero : DateTime.UtcNow - LastTime;
                int timesReady = (int)(timeElapsed.Ticks / Interval.Ticks);
                LastTime += TimeSpan.FromTicks(Interval.Ticks * timesReady);
            }
        }
    }
}