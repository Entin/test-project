using System;

namespace Entin.Test.Model.FSM
{
    internal class Transition
    {
        public Type ToState { get; }

        internal Transition(Type type)
        {
            ToState = type;
        }
    }
}