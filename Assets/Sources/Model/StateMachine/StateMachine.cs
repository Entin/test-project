using System;
using System.Collections.Generic;

namespace Entin.Test.Model.FSM
{
    public interface IStateMachine
    {
        event StateChangedDelegate OnStateChanged;
        IState CurrentState { get; }

        void Event(StateMachineTransition eventArgs);
    }

    public delegate void StateChangedDelegate(IState newState);

    internal sealed class StateMachine : IStateMachine
    {
        public event StateChangedDelegate OnStateChanged;

        private readonly Dictionary<Type, State> states = new Dictionary<Type, State>();

        private State currentState;
        public IState CurrentState => currentState;

        public void Add(State state)
        {
            Type type = state.GetType();

            if (states.ContainsKey(type))
                throw new InvalidOperationException($"The given state ({type.Name}) already present in state machine");

            states.Add(type, state);
        }

        public void Start(Type startState)
        {
            ChangeState(startState);
        }

        private void ChangeState(Type newStateType)
        {
            if (!states.ContainsKey(newStateType))
                throw new InvalidOperationException($"The given state ({newStateType.Name}) are not present in state machine");

            var newState = states[newStateType];

            currentState = newState;
            OnStateChanged?.Invoke(newState);
        }

        public void Event(StateMachineTransition eventArgs)
        {
            if (currentState == null)
                throw new InvalidOperationException("You send event, before state machine initialized");

            var transitionTo = currentState.Event(eventArgs);
            if (transitionTo == null)
                throw new InvalidOperationException($"Wrong event ({eventArgs}) from state ({currentState.GetType().Name})");

            ChangeState(transitionTo.ToState);
        }
    }
}
