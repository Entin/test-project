﻿namespace Entin.Test.Model.FSM
{
    public interface IShopState : IState { }

    internal class ShopState : State, IShopState
    {
        Transition toTown = new Transition(typeof(TownState));

        public override Transition Event(StateMachineTransition eventArgs)
        {
            if (eventArgs == StateMachineTransition.ToTown)
                return toTown;

            return base.Event(eventArgs);
        }
    }
}