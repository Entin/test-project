﻿namespace Entin.Test.Model.FSM
{
    public interface ITownState : IState { }

    internal class TownState : State, ITownState
    {
        Transition toShop = new Transition(typeof(ShopState));
        Transition toFarm = new Transition(typeof(FarmState));
        Transition toStorage = new Transition(typeof(StorageState));

        public override Transition Event(StateMachineTransition eventArgs)
        {
            if (eventArgs == StateMachineTransition.ToShop)
                return toShop;

            if (eventArgs == StateMachineTransition.ToFarm)
                return toFarm;

            if (eventArgs == StateMachineTransition.ToStorage)
                return toStorage;

            return base.Event(eventArgs);
        }
    }
}