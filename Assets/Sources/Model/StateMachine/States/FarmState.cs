﻿namespace Entin.Test.Model.FSM
{
    public interface IFarmState : IState { }

    internal class FarmState : State, IFarmState
    {
        Transition toTown = new Transition(typeof(TownState));

        public override Transition Event(StateMachineTransition eventArgs)
        {
            if (eventArgs == StateMachineTransition.ToTown)
                return toTown;

            return base.Event(eventArgs);
        }
    }
}