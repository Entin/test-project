﻿namespace Entin.Test.Model.FSM
{
    public interface IStorageState : IState { }

    internal class StorageState : State, IStorageState
    {
        Transition toTown = new Transition(typeof(TownState));

        public override Transition Event(StateMachineTransition eventArgs)
        {
            if (eventArgs == StateMachineTransition.ToTown)
                return toTown;

            return base.Event(eventArgs);
        }
    }
}