namespace Entin.Test.Model.FSM
{
    public interface IState { }

    internal abstract class State : IState
    {
        public virtual Transition Event(StateMachineTransition eventArgs)
        {
            return null;
        }
    }
}