﻿namespace Entin.Test.Model.Data
{
    [System.Serializable]
    internal class TutorialStageData
    {
        public int Type;
        public int Stage;

        public TutorialStageData(int type, int stage)
        {
            Type = type;
            Stage = stage;
        }
    }
}