﻿namespace Entin.Test.Model.Data
{
    [System.Serializable]
    internal class CurrencyData
    {
        public int GoldAmount;

        public CurrencyData(int goldAmount)
        {
            GoldAmount = goldAmount;
        }
    }
}