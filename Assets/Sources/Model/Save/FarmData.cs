﻿namespace Entin.Test.Model.Data
{
    [System.Serializable]
    internal class FarmData
    {
        public long CurrentGrowPlantTime;
        public string CurrentGrowReceiptKey;

        public FarmData(long currentGrowPlantTime, string currentGrowReceiptKey)
        {
            CurrentGrowPlantTime = currentGrowPlantTime;
            CurrentGrowReceiptKey = currentGrowReceiptKey;
        }
    }
}