using System.Linq;

namespace Entin.Test.Model.Data
{
    public interface ISave
    {
        void SaveGame();
        void ResetGame();
    }

    internal class Save : ISave
    {
        private IFileProvider file;
        private Settings.GameSettings gameSettings;
        private Shop.Shop shop;
        private Storage.Storage storage;
        private Farm.Farm farm;
        private Tutorial.Tutorial tutorial;
        private Currency.Currency currency;

        private readonly System.Random rnd = new System.Random();

        public Save(IFileProvider file,
            Settings.GameSettings gameSettings,
            Shop.Shop shop,
            Storage.Storage storage,
            Farm.Farm farm,
            Tutorial.Tutorial tutorial,
            Currency.Currency currency)
        {
            this.file = file;
            this.gameSettings = gameSettings;
            this.shop = shop;
            this.storage = storage;
            this.farm = farm;
            this.tutorial = tutorial;
            this.currency = currency;

            LoadGame();
        }

        public void SaveGame()
        {
            file.Write(GetCurrentData());
        }

        public void LoadGame(bool resetData = false)
        {
            if (file.Read(out Data data) && !resetData)
            {
                shop.SetData(data.Shop.LastUpdate, data.Shop.ItemKeys.Select(x => ItemFactory.CreateItem(x, gameSettings)));
                storage.SetData(data.Storage.ItemKeys.Select(x => ItemFactory.CreateItem(x, gameSettings)));
                farm.SetData(data.Farm.CurrentGrowPlantTime, data.Farm.CurrentGrowReceiptKey);
                tutorial.SetData(data.Tutorial.TutorialStages.ToDictionary(x => x.Type, x => x.Stage));
                currency.SetData(data.Currency.GoldAmount);
            }
            else
            {
                //default initialization
                shop.DefaultInitialize();
                storage.DefaultInitialize(ItemFactory.CreateItem(gameSettings.Items.GetRandomValue(rnd)));
                farm.DefaultInitialize();
                tutorial.DefaultInitialize();
                currency.SetData(100);
            }
        }

        private Data GetCurrentData()
        {
            return new Data(GetFarmData(),
                GetStorageData(),
                GetShopData(),
                GetCurrencyData(),
                GetTutorialData());
        }

        private FarmData GetFarmData()
        {
            var currentGrowProcess = farm.GrowProcess;
            if (currentGrowProcess == null)
                return new FarmData(0, string.Empty);

            return new FarmData(currentGrowProcess.Timer.LastTime.Ticks, currentGrowProcess.FarmReceipt.Key);
        }

        private StorageData GetStorageData()
        {
            return new StorageData(storage.Items.Select(x => x.Key).ToArray());
        }

        private ShopData GetShopData()
        {
            return new ShopData(shop.RefreshShopTimer.LastTime.Ticks, shop.Items.Select(x => x.Key).ToArray());
        }

        private TutorialData GetTutorialData()
        {
            return new TutorialData(tutorial.Infos.Select(x => new TutorialStageData((int)x.Key, x.Value.GetStage())).ToArray());
        }

        private CurrencyData GetCurrencyData()
        {
            return new CurrencyData(currency.Gold);
        }

        public void ResetGame()
        {
            LoadGame(true);
        }
    }
}