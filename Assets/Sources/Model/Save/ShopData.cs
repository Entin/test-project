﻿namespace Entin.Test.Model.Data
{
    [System.Serializable]
    internal class ShopData
    {
        public long LastUpdate;
        public string[] ItemKeys;

        public ShopData(long lastUpdate, string[] itemKeys)
        {
            LastUpdate = lastUpdate;
            ItemKeys = itemKeys;
        }
    }
}