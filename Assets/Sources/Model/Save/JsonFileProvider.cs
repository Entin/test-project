﻿using System.IO;
using UnityEngine;

namespace Entin.Test.Model.Data
{
    internal class JsonFileProvider : IFileProvider
    {
        private string FilePath => Application.persistentDataPath + "/data.json";

        public bool Read<T>(out T data) where T : class
        {
            if (File.Exists(FilePath))
            {
                var text = File.ReadAllText(FilePath);
                data = JsonUtility.FromJson<T>(text);
                return data != null;
            }
            else
            {
                data = null;
                return false;
            }
        }

        public void Write<T>(T data) where T : class
        {
            if (File.Exists(FilePath))
                File.Delete(FilePath);

            //dangerous! if there will be a crash you will lose data. TODO: firstly write file in reserve copy, and after remove
            var text = JsonUtility.ToJson(data);
            File.WriteAllText(FilePath, text);
        }
    }
}