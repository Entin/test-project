﻿namespace Entin.Test.Model.Data
{
    [System.Serializable]
    internal class TutorialData
    {
        public TutorialStageData[] TutorialStages;

        public TutorialData(TutorialStageData[] tutorialStages)
        {
            TutorialStages = tutorialStages;
        }
    }
}