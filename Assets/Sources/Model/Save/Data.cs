﻿namespace Entin.Test.Model.Data
{
    [System.Serializable]
    internal class Data
    {
        public FarmData Farm;
        public StorageData Storage;
        public ShopData Shop;
        public CurrencyData Currency;
        public TutorialData Tutorial;

        public Data(FarmData farm,
            StorageData storage,
            ShopData shop,
            CurrencyData currency,
            TutorialData tutorial)
        {
            Farm = farm;
            Storage = storage;
            Shop = shop;
            Currency = currency;
            Tutorial = tutorial;
        }
    }
}