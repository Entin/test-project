﻿namespace Entin.Test.Model.Data
{
    internal interface IFileProvider
    {
        bool Read<T>(out T data) where T : class;
        void Write<T>(T data) where T : class;
    }
}