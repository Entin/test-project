﻿namespace Entin.Test.Model.Data
{
    [System.Serializable]
    internal class StorageData
    {
        public string[] ItemKeys;

        public StorageData(string[] itemKeys)
        {
            ItemKeys = itemKeys;
        }
    }
}