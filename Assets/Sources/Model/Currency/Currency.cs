namespace Entin.Test.Model.Currency
{
    public interface ICurrency
    {
        int Gold { get; }
        event OnGoldChange OnGoldChange;

        bool CanPay(int amount);
    }

    public delegate void OnGoldChange(int gold);

    internal class Currency : ICurrency
    {
        public int Gold { get; private set; }

        public event OnGoldChange OnGoldChange;

        public void SetData(int goldAmount)
        {
            Gold = goldAmount;
            OnGoldChange?.Invoke(Gold);
        }

        public void AddGold(int value)
        {
            Gold += value;
            OnGoldChange?.Invoke(Gold);
        }

        public bool CanPay(int amount)
        {
            return amount <= Gold;
        }

        public void SubtractGold(int value)
        {
            Gold -= value;
            OnGoldChange?.Invoke(Gold);
        }
    }
}
