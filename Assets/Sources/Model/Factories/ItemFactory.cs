using Entin.Test.Model.Items;
using Entin.Test.Model.Settings;
using System.Linq;

namespace Entin.Test.Model
{
    internal static class ItemFactory
    {
        public static Item CreateItem(ItemsSettings itemSetting)
        {
            return new Item(itemSetting.Key, itemSetting.NameKey, itemSetting.DescriptionKey, itemSetting.Price, itemSetting.Sprite);
        }

        public static Item CreateItem(string key, Settings.GameSettings settings)
        {
            var itemSetting = settings.Items.First(x => x.Key == key);
            return new Item(itemSetting.Key, itemSetting.NameKey, itemSetting.DescriptionKey, itemSetting.Price, itemSetting.Sprite);
        }
    }
}