using Entin.Test.Model.Items;
using Entin.Test.Model.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using Zenject;

namespace Entin.Test.Model.Shop
{
    public interface IShop
    {
        IReadOnlyList<IItem> Items { get; }
        IItem CurrentItem { get; }

        Timer RefreshShopTimer { get; }

        event Action OnCurrentItemUpdate;
        event Action OnItemsUpdate;

        void BuyButtonPressed();
        void SelectItem(IItem item);

        void SelectFirstItemTutorial();
    }

    internal class Shop : IShop, ITickable
    {
        private readonly List<Item> items = new List<Item>();
        public IReadOnlyList<IItem> Items => items;

        private Item currentItem;
        public IItem CurrentItem => currentItem;

        private GameSettings gameSettings;
        private Storage.Storage storage;
        private Currency.Currency currency;
        private readonly Random random = new Random();

        private const int shopItemsCount = 12;

        public Timer RefreshShopTimer { get; private set; }

        public event Action OnCurrentItemUpdate;
        public event Action OnItemsUpdate;

        public Shop(GameSettings gameSettings, Storage.Storage storage, Currency.Currency currency)
        {
            this.gameSettings = gameSettings;
            this.storage = storage;
            this.currency = currency;
            var newItems = GenerateItems();
            RefreshItems(newItems);
        }

        public void SetData(long timerTicks, IEnumerable<Item> items)
        {
            RefreshShopTimer = new Timer(new DateTime(timerTicks), TimeSpan.FromSeconds(20));
            RefreshItems(items);
        }

        public void DefaultInitialize()
        {
            RefreshShopTimer = new Timer(DateTime.UtcNow, TimeSpan.FromSeconds(20));
            var newItems = GenerateItems();
            RefreshItems(newItems);
        }

        public void Tick()
        {
            if (RefreshShopTimer.IsReady)
            {
                RefreshShopTimer.Shift();
                var newItems = GenerateItems();
                RefreshItems(newItems);
            }
        }

        private List<Item> GenerateItems()
        {
            var items = new List<Item>();
            for (int i = 0; i < shopItemsCount; i++)
            {
                var itemSetting = gameSettings.Items.GetRandomValue(random);
                items.Add(ItemFactory.CreateItem(itemSetting));
            }
            return items;
        }

        private void RefreshItems(IEnumerable<Item> newItems)
        {
            items.Clear();
            items.AddRange(newItems);

            currentItem = null;

            OnItemsUpdate?.Invoke();
            OnCurrentItemUpdate?.Invoke();
        }

        public void BuyButtonPressed()
        {
            if (currentItem == null)
                throw new InvalidOperationException("Something went wrong. Current item is null.");

            if (!currency.CanPay(currentItem.Price))
                return;

            currency.SubtractGold(currentItem.Price);
            items.Remove(currentItem);
            storage.AddItem(currentItem);
            currentItem = null;

            OnItemsUpdate?.Invoke();
            OnCurrentItemUpdate?.Invoke();
        }

        public void SelectItem(IItem item)
        {
            var findedItem = items.First(x => x == item);
            currentItem = findedItem;
            OnCurrentItemUpdate?.Invoke();
        }

        public void SelectFirstItemTutorial()
        {
            var item = items.First();

            items.Remove(item);
            storage.AddItem(item);

            OnItemsUpdate?.Invoke();
            OnCurrentItemUpdate?.Invoke();
        }
    }
}
