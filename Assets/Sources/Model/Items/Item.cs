﻿using UnityEngine;

namespace Entin.Test.Model.Items
{
    public interface IItem
    {
        string Key { get; }
        string NameKey { get; }
        string DescriptionKey { get; }
        int Price { get; }
        Sprite Sprite { get; }
    }

    internal class Item : IItem
    {
        public string Key { get; private set; }
        public string NameKey { get; private set; }
        public string DescriptionKey { get; private set; }
        public int Price { get; private set; }
        public Sprite Sprite { get; private set; }

        public Item(string key, string nameKey, string descriptionKey, int price, Sprite sprite)
        {
            Key = key;
            NameKey = nameKey;
            DescriptionKey = descriptionKey;
            Price = price;
            Sprite = sprite;
        }
    }
}
