using System;
using System.Collections.Generic;
using System.Linq;

namespace Entin.Test.Model.Farm
{
    public interface IFarm
    {
        IReadOnlyList<IFarmReceipt> Receipts { get; }
        IGrowProcess GrowProcess { get; }
        event Action OnGrowProcessUpdate;

        void CollectCurrentGrow();
        void StartGrow(string receiptKey);
        void TutorialStartGrow(string receiptKey);
    }

    internal class Farm : IFarm
    {
        private readonly List<FarmReceipt> receipts = new List<FarmReceipt>();
        public IReadOnlyList<IFarmReceipt> Receipts => receipts;

        private GrowProcess growProcess;
        public IGrowProcess GrowProcess => growProcess;

        public event Action OnGrowProcessUpdate;

        private Storage.Storage storage;

        public Farm(Settings.GameSettings settings, Storage.Storage storage)
        {
            this.storage = storage;

            foreach (var farmReceiptSetting in settings.FarmReceipts)
            {
                var itemSource = ItemFactory.CreateItem(settings.Items.First(x => x.Key == farmReceiptSetting.SourceItemKey));
                var itemResult = ItemFactory.CreateItem(settings.Items.First(x => x.Key == farmReceiptSetting.ResultItemKey));

                receipts.Add(new FarmReceipt(farmReceiptSetting.Key, itemSource, itemResult, TimeSpan.FromSeconds(farmReceiptSetting.GrowTimeSeconds)));
            }
        }

        public void SetData(long currentGrowStartTime, string receiptKey)
        {
            if (string.IsNullOrEmpty(receiptKey))
                return;

            var receipt = receipts.First(x => x.Key == receiptKey);
            growProcess = new GrowProcess(receipt, new Timer(new DateTime(currentGrowStartTime), receipt.GrowTime));
            OnGrowProcessUpdate?.Invoke();
        }

        public void DefaultInitialize()
        {
            growProcess = null;
            OnGrowProcessUpdate?.Invoke();
        }

        public void StartGrow(string receiptKey)
        {
            if (growProcess != null)
                return;

            var receipt = receipts.First(x => x.Key == receiptKey);

            if (storage.Items.All(x => x.Key != receipt.Source.Key))
                return;

            storage.RemoveItem(receipt.Source.Key);

            growProcess = new GrowProcess(receipt, new Timer(DateTime.UtcNow, receipt.GrowTime));
            OnGrowProcessUpdate?.Invoke();
        }

        public void TutorialStartGrow(string receiptKey)
        {
            if (growProcess != null)
                return;

            var receipt = receipts.First(x => x.Key == receiptKey);

            growProcess = new GrowProcess(receipt, new Timer(DateTime.UtcNow, receipt.GrowTime));
            OnGrowProcessUpdate?.Invoke();
        }

        public void CollectCurrentGrow()
        {
            if (growProcess == null)
                throw new InvalidOperationException("Something went wrong, grow process is null");

            if (!growProcess.Timer.IsReady)
                return;

            storage.AddItem(growProcess.farmReceipt.result);
            growProcess = null;
            OnGrowProcessUpdate?.Invoke();
        }
    }
}
