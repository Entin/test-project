﻿using Entin.Test.Model.Items;
using System;

namespace Entin.Test.Model.Farm
{
    public interface IFarmReceipt
    {
        string Key { get; }
        IItem Source { get; }
        IItem Result { get; }
        TimeSpan GrowTime { get; }
    }

    internal class FarmReceipt : IFarmReceipt
    {
        public string Key { get; }

        private readonly Item source;
        public IItem Source => source;

        public readonly Item result;
        public IItem Result => result;

        public TimeSpan GrowTime { get; }

        public FarmReceipt(string key, Item source, Item result, TimeSpan growTime)
        {
            Key = key;
            this.source = source;
            this.result = result;
            GrowTime = growTime;
        }
    }
}
