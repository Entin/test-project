﻿using System;

namespace Entin.Test.Model.Farm
{
    public interface IGrowProcess
    {
        IFarmReceipt FarmReceipt { get; }
        Timer Timer { get; }
    }

    internal class GrowProcess : IGrowProcess
    {
        public readonly FarmReceipt farmReceipt;
        public IFarmReceipt FarmReceipt => farmReceipt;
        public Timer Timer { get; }

        public GrowProcess(FarmReceipt farmReceipt, Timer timer)
        {
            this.farmReceipt = farmReceipt;
            Timer = timer;
        }
    }
}
