using System;
using System.Collections.Generic;
using System.Linq;

namespace Entin.Test
{
    public static class IEnumerableExtentions
    {
        public static T GetRandomValue<T>(this IEnumerable<T> items, Random r)
        {
            if (items == null)
                throw new ArgumentNullException("Cannot get random element in null collection");
            int count = items.Count();
            if (count == 0)
                throw new ArgumentException("Cannot get random element in empty collection");
            return items.Skip(r.Next() % count).First();
        }
    }
}