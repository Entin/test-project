using System;
using UnityEngine;
using UnityEngine.UI;

namespace Entin.Test
{
    public static class GameObjectExtentions
    {
        public static T Clone<T>(this T origin, bool setAcitve = true) where T : Component
        {
            T item = GameObject.Instantiate(origin, origin.transform.parent) as T;
            item.transform.localPosition = Vector3.zero;
            item.transform.localScale = Vector3.one;
            item.gameObject.SetActive(setAcitve);
            return item;
        }

        public static void AddSingleAction(this Button button, Action action)
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => action?.Invoke());
        }

        public static void CopyRectTransform(this RectTransform to, RectTransform from)
        {
            to.pivot = from.pivot;
            to.position = from.position;
            to.sizeDelta = from.rect.size;
        }
    }
}