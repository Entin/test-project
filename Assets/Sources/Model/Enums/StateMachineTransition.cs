namespace Entin.Test
{
    public enum StateMachineTransition
    {
        ToTown,
        ToShop,
        ToFarm,
        ToStorage,
    }
}